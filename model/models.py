"""
Modelos
"""
class Persona:
    """
    Cuerpo de la clase Persona
    """
    def __init__(self, nombre, apellido):
        self.nombre = nombre
        self.apellido = apellido

    def imprimir(self):
        """
        Imprime nombre de la persona
        """
        print(self.apellido + ", " + self.nombre)
    