"""
Ejemplo de python
"""

from model.models import Persona

class Example:
    """
    Clase de ejemplo
    """
    def __init__(self):
        pass

    @classmethod
    def imprimir_mensaje(cls):
        """
        Metodo de ejemplo
        """
        print("Hola Mundo")

EXAMPLE = Example()
EXAMPLE.imprimir_mensaje()

PERSONA = Persona("Ariel", "Napoli")
PERSONA.imprimir()
